# Picobus Deployment Simulation 

This repository contains a simulation of the deployment of 4 satelites from the [Picobus](https://gitlab.com/librespacefoundation/picobus), which is a 8p pico-satellite deployer developed by Libre Space Foundation. The simuation is developed in Simscape Multibody. The main goal is to derive the exit velocities and Euler angles rates of 3 satellites(1p,1.5,1.5p).

## Results Explanation
The results can be found in the `Simscape_simulation/results` directory

- FIrstly, one can find the animation generated in the Mechanics explorer as .mp4 files
- The plots of linear velocities are named as `1st_cubik_vel.png`, `2nd_cubik_vel.png` ... (The 1st, 2nd corresponds to the order in which the satellites exit the Picobus)
- The plots of rotational velocities are named as `1st_cubik_omega.png`, ... 
- The plots of linear acceleration are named as `1st_cubik_acc_after_separation.png`, ... and they display the linear acceleration of the satellites after the separation has taken place
- The x,y,z axes are defined in the body reference frame corresponding to the satellite they are referring to (see pictures below)

<p float="left">
  <img src="qubik_axes.png" width="400" height="400"/>
  <img src="qubikx1.5_axes.png"  width="400" height="400"/>
</p>
