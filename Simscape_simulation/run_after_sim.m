% Extract values from simulink
% The first qubik
u_x = out.ScopeData1{1}.Values; %m/s
a_x = out.ScopeData1{2}.Values; %m/s2
u_y = out.ScopeData1{3}.Values;
a_y = out.ScopeData1{4}.Values;
u_z = out.ScopeData1{5}.Values;
a_z = out.ScopeData1{6}.Values;
wx = out.ScopeData1{7}.Values; %rad/sec
wy = out.ScopeData1{8}.Values;
wz = out.ScopeData1{9}.Values;
b_x = out.ScopeData1{10}.Values; %rad/sec2
b_y = out.ScopeData1{11}.Values;
b_z = out.ScopeData1{12}.Values;


% Differentiating the velocity manually
% u_x =[u_x.Time, u_x.Data(1,:)'];
% a_x = zeros(length(u_x)-1,1);
% for i = 1:(length(u_x)-1)
%    a_x(i) = (u_x(i+1,2) - u_x(i,2))/(u_x(i+1,1) - u_x(i,1));
% end

% Log data to .csv
% writematrix(V_x,'results/u_x1.csv');
% A_x =[a_x.Time, a_x.Data(1,:)'];
% writematrix(A_x,'results/a_x1.csv');
% V_y =[u_y.Time, u_y.Data(1,:)'];
% writematrix(V_y,'results/u_y1.csv');
% A_y =[a_y.Time, a_y.Data(1,:)'];
% writematrix(A_y,'results/a_y1.csv');
% V_z =[u_z.Time, u_z.Data(1,:)'];
% writematrix(V_z,'results/u_z1.csv');
% A_z =[a_z.Time, a_z.Data(1,:)'];
% writematrix(A_z,'results/a_z1.csv');
% Wx =[wx.Time, wx.Data(1,:)'];
% writematrix(Wx,'results/wx1.csv');
% Wy =[wy.Time, wy.Data(1,:)'];
% writematrix(Wy,'results/wy1.csv');
% Wz =[wz.Time, wz.Data(1,:)'];
% writematrix(Wz,'results/wz1.csv');
% B_x =[b_x.Time, b_x.Data(1,:)'];
% writematrix(B_x,'results/b_x1.csv');
% B_y =[b_y.Time, b_y.Data(1,:)'];
% writematrix(B_y,'results/b_y1.csv');
% B_z =[b_z.Time, b_z.Data(1,:)'];
% writematrix(B_x,'results/b_z1.csv');




% figure(1)
% plot(u_x)
% hold on
% plot(u_y)
% plot(u_z)
% title("1st Satellite to exit linear velocity")
% legend("x axis","y axis","z axis")


% figure(2)
% plot(a_x)
% hold on
% plot(a_y)
% plot(a_z)
% title("1st Satellite linear acceraration after separation")
% legend("x axis","y axis","z axis")
% ylim([-20,20])
% xlim([0.22,2])

% figure(3)
% plot(wx)
% hold on
% plot(wy)
% plot(wz)
% title("1st Satellite to exit rotational velocity")
% legend("x axis","y axis","z axis")

% figure(4)
% plot(b_x)
% hold on
% plot(b_y)
% plot(b_z)
% title("1st Satellite to exit rotational acceraration")
% legend("x axis","y axis","z axis")
% ylim([-20,20])
% xlim([0.15,2])

% % The 2nd qubik
% u_x = out.ScopeData2{1}.Values; %m/s
% a_x = out.ScopeData2{2}.Values; %m/s2
% u_y = out.ScopeData2{3}.Values;
% a_y = out.ScopeData2{4}.Values;
% u_z = out.ScopeData2{5}.Values;
% a_z = out.ScopeData2{6}.Values;
% wx = out.ScopeData2{7}.Values; %rad/sec
% wy = out.ScopeData2{8}.Values;
% wz = out.ScopeData2{9}.Values;
% b_x = out.ScopeData2{10}.Values; %rad/sec2
% b_y = out.ScopeData2{11}.Values;
% b_z = out.ScopeData2{12}.Values;

% V_x =[u_x.Time, u_x.Data(1,:)'];
% writematrix(V_x,'results/u_x2.csv');
% A_x =[a_x.Time, a_x.Data(1,:)'];
% writematrix(A_x,'results/a_x2.csv');
% V_y =[u_y.Time, u_y.Data(1,:)'];
% writematrix(V_y,'results/u_y2.csv');
% A_y =[a_y.Time, a_y.Data(1,:)'];
% writematrix(A_y,'results/a_y2.csv');
% V_z =[u_z.Time, u_z.Data(1,:)'];
% writematrix(V_z,'results/u_z2.csv');
% A_z =[a_z.Time, a_z.Data(1,:)'];
% writematrix(A_z,'results/a_z2.csv');
% Wx =[wx.Time, wx.Data(1,:)'];
% writematrix(Wx,'results/wx2.csv');
% Wy =[wy.Time, wy.Data(1,:)'];
% writematrix(Wy,'results/wy2.csv');
% Wz =[wz.Time, wz.Data(1,:)'];
% writematrix(Wz,'results/wz2.csv');
% B_x =[b_x.Time, b_x.Data(1,:)'];
% writematrix(B_x,'results/b_x2.csv');
% B_y =[b_y.Time, b_y.Data(1,:)'];
% writematrix(B_y,'results/b_y2.csv');
% B_z =[b_z.Time, b_z.Data(1,:)'];
% writematrix(B_x,'results/b_z2.csv');

% figure(5)
% plot(u_x)
% hold on
% plot(u_y)
% plot(u_z)
% title("2nd Satellite to exit linear velocity")
% hold on
% legend("x axis","y axis","z axis")
% 
% figure(6)
% plot(a_x)
% hold on
% plot(a_y)
% plot(a_z)
% title("2nd Satellite to exit linear acceraration")
% legend("x axis","y axis","z axis")
% 
% figure(7)
% plot(wx)
% hold on
% plot(wy)
% plot(wz)
% title("2nd Satellite to exit rotational velocity")
% legend("x axis","y axis","z axis")
% 
% figure(8)
% plot(b_x)
% hold on
% plot(b_y)
% plot(b_z)
% title("2nd Satellite to exit rotational acceraration")
% legend("x axis","y axis","z axis")

% The 3rd qubik
% u_x = out.ScopeData3{1}.Values; %m/s
% a_x = out.ScopeData3{2}.Values; %m/s2
% u_y = out.ScopeData3{3}.Values;
% a_y = out.ScopeData3{4}.Values;
% u_z = out.ScopeData3{5}.Values;
% a_z = out.ScopeData3{6}.Values;
% wx = out.ScopeData3{7}.Values; %rad/sec
% wy = out.ScopeData3{8}.Values;
% wz = out.ScopeData3{9}.Values;
% b_x = out.ScopeData3{10}.Values; %rad/sec2
% b_y = out.ScopeData3{11}.Values;
% b_z = out.ScopeData3{12}.Values;
% % 
% % V_x =[u_x.Time, u_x.Data(1,:)'];
% % writematrix(V_x,'results/u_x3.csv');
% % A_x =[a_x.Time, a_x.Data(1,:)'];
% % writematrix(A_x,'results/a_x3.csv');
% % V_y =[u_y.Time, u_y.Data(1,:)'];
% % writematrix(V_y,'results/u_y3.csv');
% % A_y =[a_y.Time, a_y.Data(1,:)'];
% % writematrix(A_y,'results/a_y3.csv');
% % V_z =[u_z.Time, u_z.Data(1,:)'];
% % writematrix(V_z,'results/u_z3.csv');
% % A_z =[a_z.Time, a_z.Data(1,:)'];
% % writematrix(A_z,'results/a_z3.csv');
% % Wx =[wx.Time, wx.Data(1,:)'];
% % writematrix(Wx,'results/wx3.csv');
% % Wy =[wy.Time, wy.Data(1,:)'];
% % writematrix(Wy,'results/wy3.csv');
% % Wz =[wz.Time, wz.Data(1,:)'];
% % writematrix(Wz,'results/wz3.csv');
% % B_x =[b_x.Time, b_x.Data(1,:)'];
% % writematrix(B_x,'results/b_x3.csv');
% % B_y =[b_y.Time, b_y.Data(1,:)'];
% % writematrix(B_y,'results/b_y3.csv');
% % B_z =[b_z.Time, b_z.Data(1,:)'];
% % writematrix(B_x,'results/b_z3.csv');
% 
% figure(9)
% plot(u_x)
% hold on
% plot(u_y)
% plot(u_z)
% title("2nd Satellite to exit linear velocity")
% hold on
% legend("x axis","y axis","z axis")
% 
% figure(10)
% plot(a_x)
% hold on
% plot(a_y)
% plot(a_z)
% title("2nd Satellite to exit linear acceraration")
% legend("x axis","y axis","z axis")
% ylim([-20,20])
% xlim([0.22,2])
% 
% figure(11)
% plot(wx)
% hold on
% plot(wy)
% plot(wz)
% title("2nd Satellite to exit rotational velocity")
% legend("x axis","y axis","z axis")

% figure(12)
% plot(b_x)
% hold on
% plot(b_y)
% plot(b_z)
% title("3rd Satellite to exit rotational acceraration")
% legend("x axis","y axis","z axis")
% 
% % The 4th qubik
% u_x = out.ScopeData4{1}.Values; %m/s
% a_x = out.ScopeData4{2}.Values; %m/s2
% u_y = out.ScopeData4{3}.Values;
% a_y = out.ScopeData4{4}.Values;
% u_z = out.ScopeData4{5}.Values;
% a_z = out.ScopeData4{6}.Values;
% wx = out.ScopeData4{7}.Values; %rad/sec
% wy = out.ScopeData4{8}.Values;
% wz = out.ScopeData4{9}.Values;
% b_x = out.ScopeData4{10}.Values; %rad/sec2
% b_y = out.ScopeData4{11}.Values;
% b_z = out.ScopeData4{12}.Values;
% % % 
% % V_x =[u_x.Time, u_x.Data(1,:)'];
% % writematrix(V_x,'results/u_x4.csv');
% % A_x =[a_x.Time, a_x.Data(1,:)'];
% % writematrix(A_x,'results/a_x4.csv');
% % V_y =[u_y.Time, u_y.Data(1,:)'];
% % writematrix(V_y,'results/u_y4.csv');
% % A_y =[a_y.Time, a_y.Data(1,:)'];
% % writematrix(A_y,'results/a_y4.csv');
% % V_z =[u_z.Time, u_z.Data(1,:)'];
% % writematrix(V_z,'results/u_z4.csv');
% % A_z =[a_z.Time, a_z.Data(1,:)'];
% % writematrix(A_z,'results/a_z4.csv');
% % Wx =[wx.Time, wx.Data(1,:)'];
% % writematrix(Wx,'results/wx4.csv');
% % Wy =[wy.Time, wy.Data(1,:)'];
% % writematrix(Wy,'results/wy4.csv');
% % Wz =[wz.Time, wz.Data(1,:)'];
% % writematrix(Wz,'results/wz4.csv');
% % B_x =[b_x.Time, b_x.Data(1,:)'];
% % writematrix(B_x,'results/b_x4.csv');
% % B_y =[b_y.Time, b_y.Data(1,:)'];
% % writematrix(B_y,'results/b_y4.csv');
% % B_z =[b_z.Time, b_z.Data(1,:)'];
% % writematrix(B_x,'results/b_z4.csv');
% 
figure(13)
plot(u_x)
hold on
plot(u_y)
plot(u_z)
title("3rd Satellite to exit linear velocity")
hold on
legend("x axis","y axis","z axis")

figure(14)
plot(a_x)
hold on
plot(a_y)
plot(a_z)
title("3rd Satellite to exit linear acceraration")
legend("x axis","y axis","z axis")
ylim([-20,20])
xlim([0.22,2])


figure(15)
plot(wx)
hold on
plot(wy)
plot(wz)
title("3rd Satellite to exit rotational velocity")
legend("x axis","y axis","z axis")

% figure(16)
% plot(b_x)
% hold on
% plot(b_y)
% plot(b_z)
% title("4th Satellite to exit rotational acceraration")
% legend("x axis","y axis","z axis")